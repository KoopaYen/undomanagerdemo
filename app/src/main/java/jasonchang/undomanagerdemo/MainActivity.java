package jasonchang.undomanagerdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jasonchang.undomanagerdemo.bus.ActionRedo;
import jasonchang.undomanagerdemo.bus.ActionUndo;
import jasonchang.undomanagerdemo.bus.BusProvider;

/**
 * Created by jason.chang on 16/1/25.
 */
public class MainActivity extends AppCompatActivity {

    private Operations.Operator currentOperator;
    private UndoManager undoManager;

    private int prevNumber;
    private boolean startCount = false;

    @Bind(R.id.tv_count)
    TextView tvCount;

    @OnClick({
            R.id.button0,
            R.id.button1,
            R.id.button2,
            R.id.button3,
            R.id.button4,
            R.id.button5,
            R.id.button6,
            R.id.button7,
            R.id.button8,
            R.id.button9}) void onNumberClick(Button btn) {
        if (tvCount.getText().equals(getString(R.string.number_0))) {
            tvCount.setText("");
        } else if (startCount) {
            String result = Integer.toString(operate(Integer.valueOf(String.valueOf(btn.getText()))));
            tvCount.setText(result);
            startCount = false;
            return;
        }
        tvCount.append(btn.getText());
    }

    @OnClick(R.id.buttonDivide) void onDivide(Button btn) {
        prevNumber = Integer.valueOf(tvCount.getText().toString());
        currentOperator = Operations.Operator.Divide;
        startCount = true;
    }

    @OnClick(R.id.buttonMultiple) void onMultiply(Button btn) {
        prevNumber = Integer.parseInt(String.valueOf(tvCount.getText()));
        currentOperator = Operations.Operator.Multiply;
        startCount = true;
    }

    @OnClick(R.id.buttonSubtract) void onSubtract(Button btn) {
        prevNumber = Integer.valueOf(String.valueOf(tvCount.getText()));
        currentOperator = Operations.Operator.Subtract;
        startCount = true;
    }

    @OnClick(R.id.buttonAdd) void onAdd(Button btn) {
        prevNumber = Integer.valueOf(String.valueOf(tvCount.getText()));
        currentOperator = Operations.Operator.Add;
        startCount = true;
    }

    @OnClick(R.id.buttonC) void onClean(Button btn) {
        int redoCount = undoManager.countRedos();
        for (int i=0; i<=redoCount; i++) {
            undoManager.forgetRedos(i);
        }

        int undoCount = undoManager.countUndos();
        for (int j=0; j<=undoCount; j++) {
            undoManager.forgetUndos(j);
        }

        updateBtnTxt();
        tvCount.setText(R.string.number_0);
    }

    @Bind(R.id.buttonUndo)
    Button btnUndo;

    @Bind(R.id.buttonRedo)
    Button btnRedo;

    @OnClick({
            R.id.buttonUndo,
            R.id.buttonRedo
    }) void onUndo_RedoClick(Button btn) {
        int id = btn.getId();
        switch (id) {
            case R.id.buttonUndo:
                if (undoManager.canUndo()) {
                    undoManager.undo(1);
                }
                break;
            case R.id.buttonRedo:
                if (undoManager.canRedo()) {
                    undoManager.redo(1);
                }
                break;
        }

        updateBtnTxt();
    }

    @Subscribe
    public void onUndoAction(ActionUndo action) {
        tvCount.setText(Integer.toString(action.undoNumber));
    }

    @Subscribe
    public void onRedoAction(ActionRedo action) {
        tvCount.setText(Integer.toString(action.redoNum));
    }

    private int operate(int nextNumber) {
        if (nextNumber == 0) {
            return nextNumber;
        }

        int result = 0;
        String description = null;

        switch (currentOperator) {
            case Add:
                result = prevNumber + nextNumber;
                description = "+" + nextNumber;
                break;
            case Subtract:
                result = prevNumber - nextNumber;
                description = "-" + nextNumber;
                break;
            case Multiply:
                result = prevNumber * nextNumber;
                description = "*" + nextNumber;
                break;
            case Divide:
                result = prevNumber / nextNumber;
                description = "/" + nextNumber;
                break;
        }

        undoManager.beginUpdate(description);
        undoManager.addOperation(new Operations(currentOperator, result, nextNumber));
        undoManager.endUpdate();

        updateBtnTxt();

        return result;
    }

    private void updateBtnTxt() {
        btnUndo.setText(undoManager.canUndo() ? String.format(getString(R.string.format_undo), undoManager.getUndoLabel()) : getString(R.string.undo));
        btnUndo.setEnabled(undoManager.canUndo());
        btnRedo.setText(undoManager.canRedo() ? String.format(getString(R.string.format_redo), undoManager.getRedoLabel()) : getString(R.string.redo));
        btnRedo.setEnabled(undoManager.canRedo());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(MainActivity.this);

        undoManager = new UndoManager();
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(MainActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(MainActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(MainActivity.this);
    }
}
