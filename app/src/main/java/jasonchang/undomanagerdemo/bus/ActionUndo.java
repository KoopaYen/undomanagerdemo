package jasonchang.undomanagerdemo.bus;

/**
 * Created by jason.chang on 16/1/27.
 */
public class ActionUndo {
    public final int undoNumber;

    public ActionUndo(int undoNumber) {
        this.undoNumber = undoNumber;
    }
}
