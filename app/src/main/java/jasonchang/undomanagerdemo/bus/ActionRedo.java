package jasonchang.undomanagerdemo.bus;

/**
 * Created by jason.chang on 16/1/27.
 */
public class ActionRedo {

    public final int redoNum;

    public ActionRedo(int redoNum) {
        this.redoNum = redoNum;
    }
}
