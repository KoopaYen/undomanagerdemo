package jasonchang.undomanagerdemo;

import android.os.Parcel;

import jasonchang.undomanagerdemo.bus.ActionRedo;
import jasonchang.undomanagerdemo.bus.ActionUndo;
import jasonchang.undomanagerdemo.bus.BusProvider;

/**
 * Created by jason.chang on 16/1/26.
 */
public class Operations extends UndoManager.UndoOperation {

    public enum Operator {
        Divide, Multiply, Subtract, Add
    }

    protected final Operator operator;
    protected final int number;
    protected final int result;

    public Operations(Parcel in) {
        this.operator = Operator.valueOf(in.readString());
        this.number = in.readInt();
        this.result = in.readInt();
    }

    public Operations(Operator operator, int number, int result) {
        this.operator = operator;
        this.number = number;
        this.result = result;
    }

    @Override
    public void redo() {
        BusProvider.getInstance().post(new ActionRedo(this.number));
    }

    @Override
    public void undo() {

        ActionUndo actionUndo = null;

        switch (this.operator) {
            case Divide:
                actionUndo = new ActionUndo(number * result);
                break;
            case Multiply:
                actionUndo = new ActionUndo(number / result);
                break;
            case Subtract:
                actionUndo = new ActionUndo(number + result);
                break;
            case Add:
                actionUndo = new ActionUndo(number - result);
                break;
        }

        BusProvider.getInstance().post(actionUndo);
    }

    @Override
    public void commit() {}

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.number);
        dest.writeInt(this.result);
        dest.writeString(this.operator.name());
    }

    @Override
    public int describeContents() {
        return super.describeContents();
    }

    public static final Creator<Operations> CREATOR = new Creator<Operations>() {
        @Override
        public Operations createFromParcel(Parcel source) {
            return new Operations(source);
        }

        @Override
        public Operations[] newArray(int size) {
            return new Operations[size];
        }
    };
}
